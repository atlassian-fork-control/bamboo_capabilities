define bamboo_capabilities::generate (
    Hash $capabilities,
    String $script_path,
    String $properties_path = '',
    String $target = $title,
) {
  validate_hash($capabilities)
  validate_string($script_path)
  validate_string($properties_path)

  file { $target :
    ensure  => file,
    content => template('bamboo_capabilities/capabilities.sh.erb'),
    mode    => '0755',
  }
}
